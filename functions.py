age_string = input("How old are you?")
age = int(age_string)
print("Next year, you'll be ", age + 1, "years old")

print(1, 2, 3, 4, 5, "and that's it!")

name = input("Please type your first name: ")
if len(name) > 8:
    print("You have a long name!")
else:
    print("Your name is nice and short.")

# You could also write the above like this either one will work just fine
name = input("Please type your first name: ")
name_length = len(name)
if name_length > 8:
    print("You have a long name!")
else: 
    print("Your name is nice and short.")
